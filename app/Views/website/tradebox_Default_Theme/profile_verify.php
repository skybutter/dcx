<div class="cryp_wrapper">
    <div class="profile-verify mt-4 mb-4">
        <div class="user-login form-design">
            <h3 class="user-login-title mb-4"><?php echo display('verify_profile');?></h3>
            <?php echo form_open_multipart("profile-verify") ?>
                <div class="form-group row">
                    <label for="verify_type" class="col-md-4 col-form-label"><?php echo display('verify_type') ?> <i class="text-danger">*</i></label>
                    <div class="col-md-8">
                        <select class="custom-select" name="verify_type" id="verify_type">
                            <option selected value=""><?php echo display('select_option') ?></option>
                            <option value="nid">New Identity Card (IC)</option>
                            <option value="cor">Corporate Name</option>
                        </select>
                    </div>
                </div>           
                <div class='form-group row countryl'>

                    <label for='id_number' class='col-md-4 col-form-label'>Citizenship<i class='text-danger'>*</i></label>
                    <br>
                    <div class='col-md-8'>
                        <select class='custom-select' name='city' id='city'>
                            <option><?php echo display('select_option');?></option>
                            <?php foreach ($countrys as $key => $value) { ?>
                                <option value='<?php echo $value->iso ?>' <?php echo $value->iso==@$country?'selected':null ?> ><?php echo esc($value->nicename) ?></option>
                            <?php } ?>                                            
                        </select>
                    </div>
                </div>
                <div class='form-group row country2'>

                    <label for='id_number' class='col-md-12 col-form-label'>Company Incorporated Country<i class='text-danger'>*</i></label>
                    <br>
                    <div class='col-md-12'>
                        <select class='custom-select' name='city' id='city'>
                            <option><?php echo display('select_option');?></option>
                            <?php foreach ($countrys as $key => $value) { ?>
                                <option value='<?php echo $value->iso ?>' <?php echo $value->iso==@$country?'selected':null ?> ><?php echo esc($value->nicename) ?></option>
                            <?php } ?>                                            
                        </select>
                    </div>
                </div>
                <span id="verify_field"></span>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-kingfisher-daisy"><?php echo display('submit') ?></button>
                    </div>
                </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    function confirmID() {
        var id_number = document.getElementById("id_number").value
        var cid_number = document.getElementById("cid_number").value
        if(id_number != cid_number) {
            alert('IC Not Matching!');
        }
    }
    
    function confirmCN() {
        var cn = document.getElementById("cn").value
        var ccn = document.getElementById("ccn").value
        if(cn != ccn) {
            alert('Company No. Not Matching!');
        }
    }
    
    function updateRequirements() {
      var name = document.getElementById('bo').value;
      if (name == "Sole proprietorship") {
        document.getElementById('doe').required = true;
        document.getElementById('document3').required = true;
        document.getElementById('document4').required = false;
      } else {
        document.getElementById('doe').required = false;
        document.getElementById('document3').required = false;
        document.getElementById('document4').required = true;
      }
    }
    
    function lettersNumbersCheck(name)
    {
       var regEx = /^[0-9a-zA-Z]+$/;
       if(id_number.value.match(regEx))
         {
          return true;
         }
       else
         {
         alert("Please enter letters and numbers only.");
         return false;
         }
    }
    
    $('.countryl').hide();
    $('.country2').hide();
    
    $("#verify_type").on("change", function(event) {
            event.preventDefault();
            var verify_type = $("#verify_type").val();
            if (verify_type == 'nid') {
                $('.countryl').show();
                $('.country2').hide();
            } else {
                $('.countryl').hide();
                $('.country2').show();
            }
        });
</script>