<div class="card">
    <div class="card-body">
        <div class="row">
        	<div class="col-sm-7 col-md-7">
                <div class="card-header">
                    <div class="cart-title">
                        <h5>Upload Document For Profile Verification</h5>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 card-body">
                    <?php echo form_open_multipart("backend/user/pending-user-verification/$user->user_id") ?>
    				<?php echo form_hidden('user_id', $user->user_id) ?>

                        <?php if ( $user->verify_type === 'nid' ) : ?>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 example-text-input font-weight-600">Verification Type</label>
                                <div class="col-sm-8">
                                    Individual Verification
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Full Name</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->first_name) ?></span>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Gender</label>
                                <div class="col-sm-8">
                                    <?php echo ($user->gender==1)?'Male':'Female' ?></span>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">NID</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->id_number) ?>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Document A</label>
                                <div class="col-sm-8">
                                <?php if ($user->document1) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document1; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document1; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_1"; ?>">Download File</a>
                                <?php } ?>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Document B</label>
                                <div class="col-sm-8">
                                <?php if ($user->document2) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document2; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document2; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_2"; ?>">Download File</a>	
                                <?php } ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    
                        <?php if ( $user->verify_type === 'cor' ) : ?>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 example-text-input font-weight-600">Verification Type</label>
                                <div class="col-sm-8">
                                    Corporate Verification
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Type Of Business Organization</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->bo) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Company Name</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->cname) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Company No</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->cn) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Date Of Incorporated</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->doi) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Date Of Expiration</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->doe) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Company Address</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->ca) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Company Email Address</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->cp) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Contact Person</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->ce) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Company Website</label>
                                <div class="col-sm-8">
                                    <?php echo esc($user->cw) ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Certificate of Incorporation</label>
                                <div class="col-sm-8">
                                <?php if ($user->document1) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document1; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document1; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_1"; ?>">Download File</a>
                                <?php } ?>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Statutory Forms of Company for Directors and shareholders</label>
                                <div class="col-sm-8">
                                <?php if ($user->document2) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document2; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document2; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_2"; ?>">Download File</a>	
                                <?php } ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Memorandum of Association / Status of the Company</label>
                                <div class="col-sm-8">
                                <?php if ($user->document3) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document3; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document3; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_3"; ?>">Download File</a>
                                <?php } ?>
                                </div>
                            </div>
                        	<div class="form-group row">
                                <label for="cid" class="col-sm-4 font-weight-600">Board Resolution for Private Limited and Limited Company</label>
                                <div class="col-sm-8">
                                <?php if ($user->document4) { ?>
                                    <img width="50" height="50" src="<?php echo IMAGEPATH.$user->document4; ?>" class="img-responsive"/>
                                    <a href="<?php echo IMAGEPATH.$user->document4; ?>" class="btn btn-success" download="<?php echo $user->first_name."_".$user->user_id."_4"; ?>">Download File</a>	
                                <?php } ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    
                    
                	<div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600">Upload Document</label>
                        <div class="col-sm-8">
                            <?php 
                                $date = date_create($user->date);
                                echo date_format($date,"jS F Y");  
                            ?>
                        </div>
                    </div>
                    
                	<div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600">Status</label>
                        <div class="col-sm-8">
                        	<h5>
                            <?php if ($user->verified == 0) { echo "Not Submited"; } ?>
                            <?php if ($user->verified == 1) { echo "Verified"; } ?>
                            <?php if ($user->verified == 2) { echo "Cancel"; } ?>
                            <?php if ($user->verified == 3) { echo "Processing"; } ?>
                            </h5>
                        </div>
                    </div>

                    <?php if ($user->verified==3) { ?>
    					<div>
                            <button type="submit" name="approve" class="btn btn-success w-100">Approve</button>
                        </div>
                    <?php } ?>
                    
                    <?php echo form_close() ?>
                    
                    <?php if ($user->verified==3) { ?>
                    <div class="mt-2">
                        <button class="btn btn-danger w-100" data-toggle="modal" data-target="#modalForm" >Reject</button>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>
            <div class="col-sm-5 col-md-5">
                <div class="card-header">
                    <div class="cart-title">
                        <h5><?php echo display('user_info') ?></h5>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mt-2">
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('user_id') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->user_id) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('referral_id') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->referral_id) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('language') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->language) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('firstname') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->first_name) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('lastname') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->last_name) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('email') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->email) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('mobile') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->phone) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('registered_ip') ?></label>
                        <div class="col-sm-8">
                            <?php echo esc($user->ip) ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600"><?php echo display('status') ?></label>
                        <div class="col-sm-8">
                            <?php echo ($user->status==1)?display('active'):display('inactive'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cid" class="col-sm-4 font-weight-600">Registered Date</label>
                        <div class="col-sm-8">
                            <?php 
                                $date = date_create($user->created);
                                echo date_format($date,"jS F Y");  
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="modalForm" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Reject Form</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    
                </div>
                
                <!-- Modal Body -->
                <div class="modal-body">
                    <?php echo form_open_multipart("backend/user/pending-user-verification/$user->user_id") ?>
                    <?php echo form_hidden('user_id', $user->user_id) ?>
                    <p class="statusMsg"></p>
                    <div>
                        <legend>Rejecting Reason</legend>
                        
                        <input type="checkbox" name="check_list[]" value="Missing Phone Number" checked>
                        <label for="check-1">Missing Phone Number</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Incorrect Identity Card Number" checked>
                        <label for="check-2">Incorrect Identity Card Number</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="No Selfie Photo with IC" checked>
                        <label for="check-3">No Selfie Photo with IC</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Incorrect Name" checked>
                        <label for="check-4">Incorrect Name</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Incorrect photo" checked>
                        <label for="check-5">Incorrect photo</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Identity Card not clear as can’t do verification" checked>
                        <label for="check-6">Identity Card not clear as can’t do verification</label>
                        </div>

                        <div>
                        <input type="checkbox" name="check_list[]" value="No documents attached" checked>
                        <label for="check-7">No documents attached</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="No email provided" checked>
                        <label for="check-8">No email provided</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Document not clear" checked>
                        <label for="check-9">Document not clear</label>
                        </div>
                        
                        <div>
                        <input type="checkbox" name="check_list[]" value="Business licence has expired" checked>
                        <label for="check-10">Business licence has expired</label>
                        </div>
                        
                    </div>
                    <div class="row p-3">
                        <button type="submit" name="cancel" class="btn btn-danger p-2 mx-2">Reject</button>
                        <button type="button" class="btn btn-primary p-2" data-dismiss="modal">Close</button>
                    </div>    
                    
                    <?php echo form_close() ?>
                </div>

                
            </div>
        </div>
    </div>
</div>

<script>
    
</script>