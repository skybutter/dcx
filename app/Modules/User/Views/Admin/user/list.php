<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table id="ajaxtable" class="table table-bordered table-responsive">
                <thead>
                    <tr> 
                        <th><?php echo display('sl_no') ?></th>
                        <th><?php echo display('user_id') ?></th>
                        <th><?php echo display('username') ?></th>
                        <th><?php echo display('fullname') ?></th>
                        <!--<th><?php echo display('balance') ?></th>-->
                        <th><?php echo display('email') ?></th>
                        <th><?php echo display('mobile') ?></th>
                        <th>Created Date</th>
                        <th width="180"><?php echo display('status') ?></th> 
                    </tr>
                </thead>
                <tbody>
                     
                </tbody>
            </table>
        </div>
  </div>
</div>
<script src="<?php echo BASEPATH.'assets/js/user.js' ?>" type="text/javascript"></script>