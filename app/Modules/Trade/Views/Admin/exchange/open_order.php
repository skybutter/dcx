<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-sm">
                        <thead>
                            <tr class="table-bg">
                                <th><?php echo display('trade');?></th>
                                <th class="text-right"><?php echo display('user_id');?></th>
                                <th class="text-right"><?php echo display('rate');?></th>
                                <th class="text-right"><?php echo display('required');?> Qty</th>
                                <th class="text-right"><?php echo display('available');?> Qty</th>
                                <th class="text-right"><?php echo display('required');?> Amt</th>
                                <th class="text-right"><?php echo display('available');?> Amt</th>
                                <th><?php echo display('market');?></th>
                                <th><?php echo display('open');?></th>
                                <th class="text-center"><?php echo display('status');?></th>
                            </tr>
                        </thead>
                        <tbody id="useropenTrade">
                            <?php  foreach ($open_trade as $key => $value) { ?>
                                <tr>
                                    <td><?php echo esc($value->bid_type); ?></td>
                                    <td><?php echo esc($value->user_id); ?></td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_price, 2, '.', '')); ?> USDT</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_qty, 2, '.', '')); ?></td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_qty_available, 2, '.', '')); ?></td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->total_amount, 2, '.', '')); ?> USDT</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->amount_available, 2, '.', '')); ?> USDT</td>
                                    <td>DC/USDT</td>
                                    <td><?php echo esc($value->open_order); ?></td>
                                    <td class="text-center"><button type="button" class="btn btn-primary btn-sm cursor-deafult"><i title='<?php echo display('running');?>' class='fas fa-spinner fa-pulse mr-2 fs-20 text-warning'></i><?php echo display('running');?></button></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php echo htmlspecialchars_decode($pager); ?>
                </div>
            </div> 
        </div>
    </div>
</div>

 