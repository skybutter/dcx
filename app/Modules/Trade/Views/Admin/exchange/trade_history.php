<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered table-sm">
                        <thead>
                            <tr class="table-bg">
                                <th><?php echo display('trade');?></th>
                                <th class="text-right"><?php echo display('rate');?></th>
                                <th class="text-right"><?php echo display('required');?> Qty</th>
                                <th class="text-right">Available Qty</th>
                                <th class="text-right"><?php echo display('required');?> Amt</th>
                                <th class="text-right">Available Amt</th>
                                <th><?php echo display('market');?></th>
                                <th><?php echo display('date');?></th>
                                <th class="hide text-right">Complete Qty</th>
                                <th class="hide text-right">Complete Amt</th>
                                <th><?php echo display('trade');?> <?php echo display('time');?></th>
                                <th class="text-center"><?php echo display('status');?></th>
                            </tr>
                        </thead>
                        <tbody id="usertradeHistory">
                            <?php  foreach ($trade_history as $key => $value) { ?>
                                <tr>
                                    <td><?php echo esc($value->bid_type) ?></td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_price, 2, '.', '')); ?> USDT</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_qty, 2, '.', '')); ?> DC</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->bid_qty_available, 2, '.', '')); ?> DC</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->total_amount, 2, '.', '')); ?> USDT</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->amount_available, 2, '.', '')); ?> USDT</td>
                                    <!--<td><?php echo esc($value->market_symbol) ?></td>-->
                                    <td>DC/USDT</td>
                                    <td><?php echo esc($value->open_order) ?></td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->complete_qty, 2, '.', '')); ?> DC</td>
                                    <td class="text-right"><?php echo esc(number_format((float)$value->complete_amount, 2, '.', '')); ?> USDT</td>

                                    <td><?php echo esc($value->success_time) ?></td>
                                    <?php if($value->bid_qty_available == 0){?>
                                        <td class="text-center"><i title='<?php echo display('complete');?>' class='fas fa-check mr-2 fs-20 text-success'></i></td>
                                    <?php } else { ?>
                                        <td class="text-center"><i title='<?php echo display('running');?>' class='fas fa-spinner fa-pulse mr-2 fs-20 text-warning'></i></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php echo htmlspecialchars_decode($pager); ?>
                </div>
            </div> 
        </div>
    </div>
</div>

 